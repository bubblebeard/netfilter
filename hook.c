#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/skbuff.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include<linux/netfilter_ipv4/ipt_REJECT.h>
MODULE_LICENSE("GPL");

struct nf_hook_ops hook_bind;

unsigned int Hook_Func(unsigned int hooknum, struct sk_buff *skb, const struct net_device *in,
        const struct net_device *out, int (*okfn)(struct sk_buff *)) {
    struct iphdr *ip;
    struct tcphdr *tcp;

    if (skb->protocol == htons(ETH_P_IP)) {
        ip = (struct iphdr *) skb_network_header(skb);
        if (ip->version == 4 && ip->protocol == IPPROTO_TCP) {
            skb_set_transport_header(skb, ip->ihl * 4);
            tcp = (struct tcphdr *) skb_transport_header(skb);
            if (tcp->dest == htons(22)) {
                return NF_DROP;
            }
            if (tcp->dest == htons(7778)) {
                tcp->dest = htons(7777);
                return NF_ACCEPT;
            }
        }
    }
    return NF_ACCEPT;
}

int Init(void) {
    printk(KERN_INFO "module init\n");
    hook_bind.hook = Hook_Func;
    hook_bind.pf = PF_INET;
    hook_bind.hooknum = NF_INET_PRE_ROUTING;
    hook_bind.priority = NF_IP_PRI_FIRST;
    nf_register_hook(&hook_bind);
    return 0;
}

void Exit(void) {
    nf_unregister_hook(&hook_bind);
    printk(KERN_INFO "module exit\n");
}

module_init(Init);
module_exit(Exit);