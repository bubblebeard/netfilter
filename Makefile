CURRENT = $(shell uname -r)
KDIR = /lib/modules/$(CURRENT)/build
PWD = $(shell pwd)
DEST = /lib/modules/$(CURRENT)/misc
EXTRA_CFLAGS += -std=gnu99

obj-m := hook.o 
	
default:
	$(MAKE) -C $(KDIR) M=$(PWD) modules
